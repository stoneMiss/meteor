/****************************************************
 * Description: Controller for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
	*  2019-12-05 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.cmdlog.web;
import cn.com.ry.framework.application.meteor.meteor.cmdlog.entity.CmdlogEntity;
import cn.com.ry.framework.application.meteor.meteor.cmdlog.service.CmdlogService;
import java.util.Date;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.utils.Excel2007Util;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecCreate;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecDelete;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecEdit;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/meteor/cmdlog")
public class CmdlogController extends SpringControllerSupport{
	@Autowired
	private CmdlogService cmdlogService;


	@SecPrivilege(title="t_meteor_cmdlog")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		return page;
	}

	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = cmdlogService.findPage(query,page);
		return getViewPath("list");
	}

	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("cmdlog") CmdlogEntity cmdlog,Model model){
		return getViewPath("input");
	}

	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		CmdlogEntity cmdlog = cmdlogService.getById(id);
		model.addAttribute("cmdlog",cmdlog);
		return getViewPath("input");
	}

	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	@ResponseBody
	public XjjJson save(@ModelAttribute CmdlogEntity cmdlog){

		validateSave(cmdlog);
		if(cmdlog.isNew())
		{
			//cmdlog.setCreateDate(new Date());
			cmdlogService.save(cmdlog);
		}else
		{
			cmdlogService.update(cmdlog);
		}
		return XjjJson.success("保存成功");
	}


	/**
	 * 数据校验
	 **/
	private void validateSave(CmdlogEntity cmdlog){
		//必填项校验
		// 判断服务器地址是否为空
		if(null==cmdlog.getHostname()){
			throw new ValidationException("校验失败，服务器地址不能为空！");
		}
		// 判断应用名称是否为空
		if(null==cmdlog.getAppname()){
			throw new ValidationException("校验失败，应用名称不能为空！");
		}
		// 判断命令是否为空
		if(null==cmdlog.getCmd()){
			throw new ValidationException("校验失败，命令不能为空！");
		}
		// 判断操作人员ID是否为空
		if(null==cmdlog.getCreatePersonId()){
			throw new ValidationException("校验失败，操作人员ID不能为空！");
		}
		// 判断操作人员名称是否为空
		if(null==cmdlog.getCreatePersonName()){
			throw new ValidationException("校验失败，操作人员名称不能为空！");
		}
		// 判断创建时间是否为空
		if(null==cmdlog.getCreateTime()){
			throw new ValidationException("校验失败，创建时间不能为空！");
		}
	}

	@SecDelete
	@RequestMapping("/delete/{id}")
    @ResponseBody
	public XjjJson delete(@PathVariable("id") Long id){
		cmdlogService.delete(id);
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
    @ResponseBody
	public XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			cmdlogService.delete(id);
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}

    /**
    * 导入用户
    * @param model
    * @return
    */
    @RequestMapping(value = "/import")
    public String importExcel(Model model) {
        return getViewPath("import");
    }
    /**
    * 导入
    * @param model
    * @return
    */
	@SecCreate
	@SecEdit
    @RequestMapping(value = "/import/save")
    @ResponseBody
    public  XjjJson saveImport(Model model,@RequestParam (value="fileId",required=false) Long fileId) {
        System.out.println("上传开始----");
        try {
            Map<String,Object> map = cmdlogService.saveImport(fileId);
            int allCnt = (Integer)map.get("allCnt");
            return XjjJson.success("导入成功：本次共计导入数据"+allCnt+"条");
        } catch (ValidationException e) {
            return XjjJson.error("导入失败：<br/>"+e.getMessage());
        }
    }

    /**
    * 导出用户信息
    * @param request
    * @param response
    * @return
    */
	@SecList
    @RequestMapping(value = "/export/excel")
    public String exportExcel(HttpServletRequest request,HttpServletResponse response) {
        List<CmdlogEntity>  userList =cmdlogService.findAll();
        LinkedHashMap<String, String> columns = new LinkedHashMap<String, String>();
        columns.put("id", "id");
        columns.put("hostname", "服务器地址");
        columns.put("appname", "应用名称");
        columns.put("cmd", "命令");
        columns.put("createPersonId", "操作人员ID");
        columns.put("createPersonName", "操作人员名称");
        columns.put("createTime", "创建时间");
        Excel2007Util.write(userList, columns,response,"cmdlog-export");
        return null;
    }
}

