/****************************************************
 * Description: Controller for t_meteor_machineinfo_privilege
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-03 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.machineinfo.web;

import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecFunction;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoPrivilegeEntity;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.service.MachineinfoPrivilegeService;
import cn.com.ry.framework.application.meteor.sec.entity.XjjUser;
import cn.com.ry.framework.application.meteor.sec.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/meteor/machineinfo/privilege")
public class MachineinfoPrivilegeController extends SpringControllerSupport {
    @Autowired
    private MachineinfoPrivilegeService machineinfoPrivilegeService;
    @Autowired
    private UserService userService;

    @SecFunction(title = "设置权限", code = "allot")
    @RequestMapping("/allot/{machineId}")
    public String allot(@PathVariable("machineId") Long machineId, Model model) {
        //获取用户列表
        List<XjjUser> userList = userService.findAll();
        model.addAttribute("userList", userList);
        //machineId
        model.addAttribute("machineId", machineId);
        //已有用户权限
        XJJParameter query = new XJJParameter();
        query.addQuery("query.machineId@eq@l", machineId);
        List<MachineinfoPrivilegeEntity> machineinfoPrivilegeServiceList = machineinfoPrivilegeService.findList(query);
        List<Integer> userIdList = new ArrayList<Integer>();
        if (machineinfoPrivilegeServiceList != null && machineinfoPrivilegeServiceList.size() > 0) {
            for (MachineinfoPrivilegeEntity tmpMachineinfoPrivilegeEntity : machineinfoPrivilegeServiceList) {
                userIdList.add(tmpMachineinfoPrivilegeEntity.getUserId());
            }
        }
        model.addAttribute("userIdList", userIdList);
        return getViewPath("allot");
    }


    /**
     * 添加权限
     *
     * @param model
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public XjjJson save(@RequestBody Map map, Model model) {
        if (null != map) {
            String strMachineId = (String) map.get("machineId");
            List<String> userIdList = (List<String>) map.get("userId");
            if (null != strMachineId && null != userIdList && userIdList.size() > 0) {
                List<MachineinfoPrivilegeEntity> machineinfoPrivilegeEntityList = new ArrayList<>();
                for (String strUserId : userIdList) {
                    MachineinfoPrivilegeEntity machineinfoPrivilegeEntity = new MachineinfoPrivilegeEntity();
                    machineinfoPrivilegeEntity.setUserId(Integer.parseInt(strUserId));
                    machineinfoPrivilegeEntity.setMachineId(Integer.parseInt(strMachineId));
                    machineinfoPrivilegeEntity.setCreateTime(new Date());
                    machineinfoPrivilegeEntityList.add(machineinfoPrivilegeEntity);
                }
                boolean flag = machineinfoPrivilegeService.batchInsert(machineinfoPrivilegeEntityList);
                if (flag) {
                    return XjjJson.success("权限添加成功");
                }else{
                    return XjjJson.error("权限添加失败");
                }
            }
        }
        return XjjJson.success("数据为空");
    }


    /**
     * 添加权限
     *
     * @param roleId
     * @param priCode
     * @param functions 多个用|分开
     * @param model
     * @return
     */
    @RequestMapping("/cancle/{roleId}/{priCode}/{functions}")
    public @ResponseBody
    XjjJson cancelPri(@PathVariable("roleId") Long roleId,
                      @PathVariable("priCode") String priCode,
                      @PathVariable("functions") String functions,
                      Model model) {

        return XjjJson.success("权限成功");
    }
}

