package cn.com.ry.framework.application.meteor.framework.websocket;

import org.java_websocket.drafts.Draft_6455;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Scanner;

public class TestWebSocket {

    public TestWebSocket(WebSocketClient client){
        client.send("{action: 'resize', cols: 113, rows: 70}");
    }
    public String send(WebSocketClient client) {
        String result = "";
        try {

            Scanner scanner = new Scanner(System.in);
            while (true) {
                String commondStr = scanner.nextLine();

                String json="{action:'read',data:'"+commondStr+"\r'}\r\n";
//                json = "{action:'read',data:'d'}";
//                client.send(json.getBytes().toString());
//                json = "{action:'read',data:'\r'}";
//                System.out.println(json);
                client.send(json.getBytes().toString());


                if (commondStr.equals("exit")) {
                    scanner.close();
                    client.close();
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            client.close();
        }
        return result;
    }

    public static void main(String[] args) throws URISyntaxException, InterruptedException {
        String serverUrl = "ws://192.168.23.128:8563/ws";
        URI recognizeUri = new URI(serverUrl);
        WebSocketClient client = new WebSocketClient(recognizeUri, new Draft_6455());
        client.connect();
        Thread.sleep(2000);
        TestWebSocket testWebSocket = new TestWebSocket(client);
        testWebSocket.send(client);
//        client.close();
    }
}
