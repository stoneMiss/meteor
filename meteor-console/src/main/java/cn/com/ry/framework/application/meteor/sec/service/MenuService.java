/****************************************************
 * Description: Service for 菜单
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.service;

import cn.com.ry.framework.application.meteor.sec.entity.MenuEntity;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;

import java.util.List;

public interface MenuService  extends XjjService<MenuEntity> {


	public List<MenuEntity> findMenusByPid(Long pid);
	public List<MenuEntity> findAllValid();

	/**
	 * 根据用户角色获得用户的菜单
	 * @param roleIds
	 * @return
	 */
	public List<MenuEntity> findMenusByRoleIds(Long[] roleIds);
}
