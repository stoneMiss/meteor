/****************************************************
 * Description: Service for ${model.label}
 * Copyright:   Copyright (c) ${model.year}
 * Company:     ${model.company}
 * @author      ${model.author}
 * @version     ${model.version}
 * @see
	HISTORY
    *  ${model.date} ${model.author} Create File
**************************************************/
package ${model.packageForService};
import ${model.packageForModel}.${model.name?cap_first}Entity;
import ${model.globalPackage}.framework.service.XjjService;
import ${model.globalPackage}.framework.exception.ValidationException;

import java.util.Map;


public interface ${model.name?cap_first}Service  extends XjjService<${model.name?cap_first}Entity>{

    /**
    * 导入
    * @param fileId
    */
    public Map<String,Object> saveImport(Long fileId) throws ValidationException;
}
