${'<#--'}
/****************************************************
* Description: ${model.label}的列表页面
* Copyright:   Copyright (c) ${model.year}
* Company:     ${model.company}
* @author      ${model.author}
* @version     ${model.version}
* @see
HISTORY
*  ${model.date} ${model.author} Create File
**************************************************/
${'-->'}
<${'#'}include "/templates/xjj-index.ftl">

<${'@'}input url="${'$'}{base}${model.requestMapping}/import/save" id=tabId>

 	<${'@'}formgroup title='导入文件'>
    	<${'@'}upload uploadPath="/${model.module?uncap_first}/${model.name?uncap_first}" fileInfo=fileInfo multiple='false' sizeLimit="2097152" callBackFunc="" allowedExtensions="['xls']" buttonText="请选择文件"/>
    	1、请上传xls格式的excel文件；<br />
    	2、严格按照模版格式填写导入文件；<br />
    	3、点击确定开始导入。<br />
    	&nbsp;&nbsp;&nbsp;&nbsp;<a href='${'$'}{base}/import-template/${model.module?uncap_first}/${model.name?uncap_first}Excel.xls' ><font  style='font-size:16px;' color='red'><b>下载模版</b></font></a>
    </${'@'}formgroup>

</${'@'}input>
