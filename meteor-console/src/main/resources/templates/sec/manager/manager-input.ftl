<#include "/templates/xjj-index.ftl">

<@input url="${base}/sec/manager/save" id=tabId>
    <input type="hidden" name="id" value="${user.id}"/>
    <@formgroup title='类型'>
		<@select name="userType" list=XJJConstants.USER_TYPE value=user.userType emptyOption=false></@select>
    </@formgroup>
    <@formgroup title='账号'><input type="text" name="loginName" value="${user.loginName}" check-type='required'></@formgroup>
    <@formgroup title='姓名'><input type="text" name="userName" value="${user.userName}" check-type='required'></@formgroup>
    <@formgroup title='密码'><input type="text" name="password" placeholder="默认:123456"></@formgroup>
    <@formgroup title='手机'><input type="text" name="mobile" value="${user.mobile}" check-type='required'></@formgroup>
    <@formgroup title='邮箱'><input type="text" name="email" value="${user.email}" check-type='required'></@formgroup>
    <@formgroup title='状态'>
		<@swichInForm name="status" val=user.status onTitle="有效" offTitle="无效"></@swichInForm>
    </@formgroup>

</@input>
