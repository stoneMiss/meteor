<#--
/****************************************************
* Description: 服务器信息的列表页面
* Copyright:   Copyright (c) 2019
* Company:     ry
* @author      reywong
* @version     1.0
* @see
HISTORY
*  2019-12-04 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">

<@input url="${base}/meteor/machineinfo/import/save" id=tabId>

 	<@formgroup title='导入文件'>
    	<@upload uploadPath="/meteor/machineinfo" fileInfo=fileInfo multiple='false' sizeLimit="2097152" callBackFunc="" allowedExtensions="['xls']" buttonText="请选择文件"/>
    	1、请上传xls格式的excel文件；<br />
    	2、严格按照模版格式填写导入文件；<br />
    	3、点击确定开始导入。<br />
    	&nbsp;&nbsp;&nbsp;&nbsp;<a href='${base}/import-template/meteor/machineinfoExcel.xls' ><font  style='font-size:16px;' color='red'><b>下载模版</b></font></a>
    </@formgroup>

</@input>
