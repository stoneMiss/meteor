<#--
/****************************************************
 * Description: 全局参数设置
 * Copyright:   Copyright (c) 2018
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-12 RY Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@input>
    <@formgroup title='模块'>
        <@select id="moduleName" list=moduleNameList listKey='key' listValue='value'  onChange="getHostList()"></@select>
    </@formgroup>
    <@formgroup title='服务器地址'>
        <select id="hostname">
            <option value=''>请选择</option>
        </select>
    </@formgroup>
    <@formgroup>
        <@button type="purple" icon="fa fa-pencil" onclick="makeSession()">开启</@button>
    </@formgroup>
</@input>

<script type="text/javascript">

    //获取服务器列表
    function getHostList() {
        //清空hostName
        $("#hostname").html("<option value=''>请选择</option>");

        var moduleName = $("#moduleName").val();
        var url = "${base}/meteor/param/getMachineList/" + moduleName;
        $.ajax({
            type: "GET",
            async: false,
            dataType: "JSON",
            contentType: "application/json;charset=UTF-8",
            url: url,
            success: function (data) {
                if (data != null && data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        //构建hostlist
                        $("#hostname").append("<option value='" + data[i].id + "'>" + data[i].hostname + "</option>");
                    }

                }
            },
            error:
                function (e) {
                    XJJ.msger(e.responseText);
                    console.log(e.status);
                    console.log(e.responseText);
                }

        });
    }

    //构建当前session
    function makeSession() {
        var moduleName = $("#moduleName").val();
        var machineId = $("#hostname").val();
        if (moduleName == "") {
            XJJ.msger("请输入模块名称");
            return;
        }
        if (machineId == "") {
            XJJ.msger("请输入机器名称");
            return;
        }
        var url = "${base}/meteor/param/saveMachineParam";
        var data = {
            "id": parseInt(machineId),
            "moduleName": moduleName
        };
        $.ajax({
            type: "POST",
            async: false,
            dataType: "JSON",
            contentType: "application/json;charset=UTF-8",
            url: url,
            data: JSON.stringify(data),
            success: function (data) {
                if (data.message != '' && data.type == 'success') {
                    XJJ.msgok(data.message);
                    //启动
                    showArthasConsole();

                    setTimeout(function () {
                        if(arthas_ws!=null){
                            //跳转到工作台
                            XJJ.showTab({id:'meteor_workplatform',text:'工作台',url:'/meteor/workplatform/index',navs:'Meteor,工作台'});
                        }
                    },1000)

                } else {
                    XJJ.msger(data.message);
                }
            },
            error:
                function (e) {
                    XJJ.msger(e.responseText);
                    console.log(e.status);
                    console.log(e.responseText);
                }

        });
    }

    function showArthasConsole() {
        //显示控制台帮助文档
        var options = {
            id: 'meteor_arthas',
            text: 'arthas执行器',
            url: '${base}/meteor/arthas/index',
            navs: 'meteor,arthas执行器',
            closeable: false,
            show: false
        };
        var tOpions = $.extend({}, XJJ.tabOptions, options);
        XJJ.showTab(tOpions);

    }
</script>

